%clear all;
%close all;

format short eng

Vi = 300;
Vo = 100;
Po = 1e3; Ro = (Vo^2)/Po;
L = 500e-6;
C = 100e-6;
Rse = 0.5;
Vs = 10;
Rl = 0;

% 1 - Caracteristica estatica

num = [Rse*C 1];
den = L*C.*[1 (1/(Ro*C) + (Rse + Rl)/L) (1/(L*C))];

pkg load control

g = tf(num,den)

[mag, pha, w]  = bode(g);
bode(g);

% 1 - d) Calculo do compensador

fc_desejada = 2e3;
w_desejado = 2*pi*fc_desejada;
fase_desejada = 70;

[dummy,w_i] = min(abs(w-w_desejado));

mag_em_fc = mag(w_i); mag_em_fc_db = 20*log10(mag_em_fc);
pha_em_fc = pha(w_i)

%avanco = fase_desejada - pha_em_fc - 90;
avanco = 80;
mag_em_fc_db = -20;

% conversao para radianos
avanco = avanco*pi/180;

global k = ( tan(avanco/4 + pi/4) )^2

R1 = 10e3;
G = 10^(-mag_em_fc_db/20)

% C1 + C2 = 10e-9

f = 2e3;
C2 = 1/(2*pi*f*G*R1);
C1 = C2*(k-1);
R2 = sqrt(k)/(2*pi*f*C1);
R3 = R1/(k-1);
C3 = 1/(2*pi*f*R3*sqrt(k));

R1
R2
R3
C1
C2
C3
f


