clear all;
close all;

format short eng;

global Lm = 1.0/(2*pi*1e3);
%global Lm = 80e-6;
Lm

function y = f(x)
  global Lm;
  y = zeros(1,1);
  y(1) = inv(2*pi*sqrt(Lm*x(1)*50^2))-30e3;
  %y(1) = inv(2*pi*sqrt(Lm*x(1)))-30e3;
endfunction
[Vo, fval, info] = fsolve(@f,[70e-12]);
global Cs = Vo(1)
Cs
function y = g(x)
  global Cs;
  y = zeros(1,1);
  y(1) = inv(2*pi*sqrt(Cs*x(1)*50^2))-300e3;
endfunction
[Vo, fval, info] = fsolve(@g,[70e-12]);
Lp = Vo(1)
Lm2 = Lm*50^2

