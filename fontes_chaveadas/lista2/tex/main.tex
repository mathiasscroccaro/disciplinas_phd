\documentclass{article}[12pt]
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage[portuguese]{babel}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{steinmetz}
\usepackage{tikz}
\usepackage{multicol}

\usepackage{sectsty}

%%% --- HIPERLINKS --- %%%

\usepackage[colorlinks=true,plainpages=true,citecolor=black,linkcolor=black, urlcolor=blue]{hyperref}

\usepackage{indentfirst}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage[a4paper,
 top=3cm, 
 bottom=2cm, 
 left=3cm, 
 right=2cm]{geometry} 
 
\usepackage{titlesec}
\usepackage{helvet}

\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}


\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    frame=single,
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

%%% --- CABEÇALHO E RODAPÉ --- %%%

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}

\fancyhead[L]{\footnotesize Lista de exercícios 2}
\fancyhead[R]{\footnotesize IT505A}   
\fancyfoot[R]{\footnotesize Unicamp} 
\fancyfoot[C]{\thepage} 
\fancyfoot[L]{\footnotesize Mathias Scroccaro Costa - RA 210322} 
\renewcommand{\footrulewidth}{0.7pt}
\renewcommand{\headrulewidth}{0.7pt}
 

%\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\baselinestretch}{1.5} 
\titlespacing{\section}{0pt}{36pt}{24pt}
\setlength{\parindent}{4em}


\title{IT505A - Lista de exercícios 2}

\def \proporcaoFigura {0.8}

\begin{document}

\begin{titlepage}
\begin{center}

\LARGE

\vspace{3cm}
Lista de exercícios 2
\Large

\vspace{5cm}
 
\textbf{MATHIAS SCROCCARO COSTA - RA 210322}

\vspace{7cm}

\today

\vspace{5cm}

\large
Faculdade de Engenharia Elétrica e Computação – FEEC\\
Departamento de Semicondutores, Instrumentos e Fotônica – DSIF\\
UNICAMP, Campinas
\vspace*{\fill}
\end{center}
\end{titlepage}

\newpage

\normalsize

\sectionfont{\fontsize{12}{15}\selectfont}

\titleformat{\section}
{\normalfont\LARGE\bfseries}{\thesection}{1em}{}


\section*{Questão 1}

\subsection*{Simule o circuito abaixo com uma frequência de chaveamento de 25 kHz, largura de pulso de 50\% (fonte Vg com tensões de +/- 12 V). A relação de espiras do elemento magnético é de 1:10. Analise os valores das grandezas listadas abaixo e verifique se o resultado da simulação é consistente com as expectativas teóricas. Em caso de discrepância, procure justificar as diferenças. Simule pelo menos 5ms.}

O esquemático mostrado na Figura \ref{fig.1} foi simulado no \textit{software} Orcad 16.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{imagens/flyback_esquematico.png}
\caption{Esquemático do circuito flyback simulado.}
\label{fig.1}
\end{figure}

\subsection*{a) Tensão de saída.}

A tensão de saída esperada é encontrada através da Equação \eqref{eq.flyback.vo}, com valor Vo = 100 V. 

\begin{align}
Vo = \dfrac{N2}{N1} \cdot \dfrac{E \cdot \delta}{(1-\delta)} = 100~V
\label{eq.flyback.vo}
\end{align} 

Na Figura \ref{fig.flyback.vo} é mostrada a tensão sobre a carga, com valor médio de Vo = 92 V. Há uma pequena discrepância entre o valor calculado e simulado devido às percas que recaem sobre o transistor IRF150 e diodo. Algumas dezenas de amperes fluem no enrolamento do primário e atravessam o canal do transistor. Isto resulta numa queda de tensão sobre o canal, diminuindo a tensão sobre os enrolamentos do primário e, consequentemente, diminuindo a tensão no secundário e carga.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_tensao.png}
\caption{Tensão de saída do circuito.}
\label{fig.flyback.vo}
\end{figure}

\subsection*{b) Ondulação da tensão de saída.}

O \textit{ripple} na tensão de saída esperado é obtido através da Equação \eqref{eq.flyback.ripple.vo}, com valor $\Delta$Vo = 4 V. 

\begin{align}
\Delta Vo = \dfrac{Io(max) \cdot \tau \cdot \delta}{Co} = 4~V
\label{eq.flyback.ripple.vo}
\end{align} 

Na Figura \ref{fig.flyback.ripple.vo} é mostrado o \textit{ripple} na tensão sobre a carga, com valor $\Delta$Vo = 3,67 V. Há uma pequena discrepância entre os valores calculados e simulados devido às percas que recaem sobre o transistor e diodo.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_ripple_tensao.png}
\caption{Ripple na tensão de saída do circuito.}
\label{fig.flyback.ripple.vo}
\end{figure}

\subsection*{c) Tensão Vds do transistor.}

Idealmente, a forma de onda esperada sobre o dreno do transistor, Vds, é uma onda quadrada com amplitude máxima igual à somatória da tensão de entrada mais a tensão de saída refletida ao primário, neste caso, $Vds(max) = 10+10 = 20~V$. Na Figura \ref{fig.flyback.vds} é mostrada esta tensão durante a simulação.


\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_vds.png}
\caption{Tensão de observada sobre o dreno do transistor.}
\label{fig.flyback.vds}
\end{figure}

\subsection*{d) Ondulação da corrente em L1 e em L2 (considere apenas os intervalos em que há corrente no transistor e no diodo, respectivamente).}

Para o cálculo da variação de corrente observada sobre o indutor do primário, foi analisado o comportamento de L1 quando o transistor está conduzindo. A tensão que recai em seus terminais E, pelo período $t_T ~=~ \tau \cdot \delta$, resulta numa variação de corrente $di$, como indicado na Equação \eqref{eq.flyback.ripple.corrente}. O valor encontrado de variação de corrente foi de $\Delta I_{L1}$ = 4 A. A variação de corrente em L2 é encontrada pela ponderação do número de espiras, de maneira que $I_{L2}$ = 0.4 A.

\begin{align}
V ~=~ L \dfrac{di}{dt} ~\longrightarrow~ di ~=~ \dfrac{V \cdot dt}{L} ~=~ \dfrac{E \cdot \tau \cdot \delta}{L} ~=~ 4~A
\label{eq.flyback.ripple.corrente}
\end{align}

Através da simulação, como mostrado na Figura \ref{fig.flyback.ripple.indutores}, foi observado \textit{ripple} na corrente do indutor L1 de $\Delta I_{L1}=3,55$ A e no indutor L2 de $\Delta I_{L2}=0,35$ A.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_ripple_indutores.png}
\caption{Variação de corrente observada sobre os indutores do primário (cor vermelho claro) e secundário (cor azul).}
\label{fig.flyback.ripple.indutores}
\end{figure}

\subsection*{e) Altere o acoplamento dos indutores para 0,95 e repita a simulação e as análises anteriores, justificando as eventuais alterações de resultados.}

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_ripple_tensao_095.png}
\caption{Tensão de saída do circuito.}
\label{fig.flyback.vo.095}
\end{figure}

Na Figura \ref{fig.flyback.vo.095} é mostrada a tensão de saída Vo observada durante a simulação. Em relação ao transitório, o sistema apresenta uma dinâmica mais rápida, atingindo mais rapidamente o regime permanente. Quando o atinge, a tensão média é menor, comparado ao acoplamento indutivo ideal, com valor de Vo = 63,5 V. O \textit{ripple} observado é de cerca de $\Delta$Vo = 2,1 V.

Na Figura \ref{fig.flyback.vds.095} é mostrada a tensão sobre o dreno do transistor Vds. No momento que o transistor deixa de conduzir, há um pico de tensão seguido de uma oscilação, que não acontece no acoplamento ideal de indutores. O pico ocorre devido ao carregamento repentino das capacitâncias do modelo do transistor pela corrente armazenada nas indutâncias de dispersão. As oscilações ocorrem devido a ressonância das capacitâncias do modelo do transistor e a indutância de dispersão. 

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_vds_095.png}
\caption{Tensão de observada sobre o dreno do transistor.}
\label{fig.flyback.vds.095}
\end{figure}

Na Figura \ref{fig.flyback.ripple.indutores.095} é mostrada a variação de corrente no L1 e L2, quando o transistor e o diodo são acionados, respectivamente. Comparando ao acoplamento ideal de indutores, a forma de onda distorceu, mas com valores de \textit{ripple} semelhantes. Há uma nova oscilação em alta frequência, principalmente quando é cessada a corrente no indutor.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/flyback_ripple_indutores_095.png}
\caption{Variação de corrente observada sobre os indutores do primário (cor vermelho claro) e secundário (cor azul).}
\label{fig.flyback.ripple.indutores.095}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage

\section*{Questão 2}

\subsection*{Simule o circuito a seguir que se refere à aplicação de um sinal MLP a um filtro LC e carga resistiva. Observe e comente o comportamento da corrente no indutor e da tensão de saída no transitório de partida (condições iniciais nulas) e quando ocorre a alteração na carga. A tensão de alimentação do uA741 é +/- 15V. A onda portadora (Vport) é de 1 kHz, variando entre 0 e 5 V.}

O esquemático mostrado na Figura \ref{fig.mlp.esquematico} foi simulado no \textit{software} Orcad 16.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/MLP_esquematico.png}
\caption{Esquemático do circuito com chaveamento MLP.}
\label{fig.mlp.esquematico}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/MLP_corrente_tensao.png}
\caption{Tensão observada sobre a carga (traço cor verde) e corrente vista sobre o indutor (cor vermelha).}
\label{fig.mlp.corrente.tensao}
\end{figure}

Na Figura \ref{fig.mlp.corrente.tensao} é exibida a corrente no indutor (sinal com coloração vermelha) e também a tensão sobre a carga (cor verde). Durante os transitórios, tanto a tensão quanto a corrente média sofrem oscilações, mostrando que o conversor com MLP é sensível às mudanças da carga.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Questão 3}

\subsection*{Faça a simulação do circuito abaixo que realiza modulação por limites de corrente (histerese). Verifique e comente o comportamento da corrente no indutor e da tensão de saída no transitório inicial (condições iniciais nulas) e quando ocorre a alteração na carga. Compare e comente as diferenças com os resultados MLP. Analise comparativamente os espectros da tensão na saída do bloco limitador e da corrente no indutor considerando (entre 0 e 10 kHz).}

O esquemático mostrado na Figura \ref{fig.mlc.esquematico} foi simulado no \textit{software} Orcad 16.

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{imagens/MLC_esquematico.png}
\caption{Esquemático do circuito com chaveamento MLC.}
\label{fig.mlc.esquematico}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/MLC_corrente_tensao.png}
\caption{Tensão observada sobre a carga (traço cor verde) e corrente vista sobre o indutor (cor vermelha).}
\label{fig.mlc.corrente.tensao}
\end{figure}

Na Figura \ref{fig.mlc.corrente.tensao} é exibida a corrente no indutor (sinal com coloração vermelha) e também a tensão sobre a carga (cor verde). Durante os transitórios, a corrente média não sofre oscilações, mostrando que este conversor, com MLC, funciona como uma fonte de corrente constante estável. A tensão sobre a carga muda proporcionalmente à variação da resistência.

\subsubsection*{Comparação dos espectros}

Os sinais de tensão após o bloco limitador (cor verde) e corrente sobre o indutor (cor vermelha) foram observados para ambas as simulações, MLP à esquerda e MLC à direita, como exibido na Figura \ref{fig.comparacao}.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/comparacao_MLP_MLC.png}
\caption{Sinal de tensão (cor verde), visto após bloco limitador, e corrente (cor vermelha), observada sobre o indutor. À esquerda MLP e à direita MLC.}
\label{fig.comparacao}
\end{figure}

Os espectros destes sinais foram obtidos. Primeiramente, o espectro da corrente, mostrado na Figura \ref{fig.espectro.corrente}. À esquerda do MLP e à direita MLC. A forma do espectro de MLP remete ao espectro de uma onda triangular, característico por várias raias harmonicamente relacionadas, convoluido ao espectro de um degrau amortecido, característico pelo decaimento `exponencial'. Este comportamento no espectro é visualizado no domínio do tempo. Em relação ao MLC, o espectro é mais distribuído, devido à alteração na frequência de chaveamento necessária para o controle de corrente, no entanto, a energia de mais altas frequências decai mais rapidamente, comparado ao MLP.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/comparacao_corrente.png}
\caption{Espectro da corrente sobre o indutor. À esquerda MLP e à direita MLC.}
\label{fig.espectro.corrente}
\end{figure}

A segunda análise que foi feita foi através do espectro da tensão, mostrado na Figura \ref{fig.espectro.tensao}. À esquerda do MLP e à direita MLC. Para o MLP, o comportamento do espectro é similar ao espectro de uma onda quadrada com tensão DC somada, como era de se esperar. O espectro do MLC, apesar de possuir algumas raias harmônicas entre si, é mais distribuído, mostrando a variação da frequência de chaveamento necessária em seu controle. Comparativamente, o pico das harmônicas em altas frequências do MLC é menor ao MLP, no entanto, apresenta mais componentes espectrais. 

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{imagens/comparacao_tensao.png}
\caption{Espectro da tensão após bloco limitador. À esquerda MLP e à direita MLC.}
\label{fig.espectro.tensao}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Questão 4}

\subsection*{Para o conversor forward, com três enrolamentos, N1=100, N2=50 (enrolamento de desmagnetização), Tensão de entrada E=200 V. N3 (número de espiras do enrolamento de saída) não é conhecido. Suponha condução contínua no indutor de saída e rendimento 100\%.}

\subsection*{a) Desenhe a forma de onda em N2, para a situação de máximo ciclo de trabalho, indicando valores na escala vertical.}

A forma de onda da tensão que recai sobre o enrolamento N2, considerando máximo ciclo de trabalho, é a seguinte:

\begin{center}

%\begin{tikzpicture}
%
%\draw[->] (0,0) -- (5,0) node[anchor=north] {$t$};
%\draw[->] (0,-1) -- (0,2) node[anchor=east] {$I_{N2}$};
%\draw[->] (1.5,0.7) -- (2,1.5);
%\draw[thick,dashed] (0,1) -- (1,1);
%
%\draw	(-0.6,1) node {$Ipk_{N2}$}
%		(1.2,-0.2) node {\small $t_T$}
%		(2.1,-0.2) node {\small $\tau$}
%		(3,1.8) node {\small Slope = $-\dfrac{E \cdot N1}{N2 \cdot L_{N2}}$}
%		(3.1,-0.2) node {\small $\tau+t_T$}
%		(4.1,-0.2) node {\small $\cdot \cdot \cdot$};
%
%\draw[thick,line width = 0.08cm] (0,0) -- (1,0) -- (1,1) -- (2,0) -- (3,0) -- (3,1) -- (4,0);
%
%\end{tikzpicture}

\begin{tikzpicture}

\draw[->] (0,0) -- (3,0) node[anchor=north] {$t$};
\draw[->] (0,-2) -- (0,2) node[anchor=east] {$V_{L2}$};
\draw[thick,dashed] (0,-1) -- (1,-1);

\draw	(-0.4,1) node {E}
		(-0.9,-0.9) node {$- \dfrac{E \cdot N2}{N1}$}
		(1.2,0.2) node {\small $t_T$}
		(2.2,0.2) node {\small $\tau$};

\draw[thick,line width = 0.08cm] (0,-1) -- (1,-1) -- (1,1) -- (2,1) -- (2,-1) -- (3,-1);

\end{tikzpicture}

\end{center}

%$Ipk_{N2}$ é a corrente de pico sobre o enrolamento N2, que possui valor proporcional à corrente de pico no N1 ($Ipk_{N1}$) ponderada pelo número de espiras, ou seja, 
%\begin{align*}
%Ipk_{N2} ~=~ Ipk_{N1} \dfrac{N1}{N2}
%\end{align*}
%
%Como o valor das indutâncias não é fornecido pelo exercício, não é possível calcular o valor aproximado da corrente de pico.

\subsection*{b) Determine o máximo ciclo de trabalho.}

A determinação do máximo ciclo de trabalho pode ser obtido igualando as áreas de funcionamento do indutor do primário, como mostrado abaixo. O valor máximo encontrado foi de $\delta < 2/3$.

\begin{multicols}{2}

\begin{center}

\begin{tikzpicture}

\draw[->] (0,0) -- (3,0) node[anchor=north] {$t$};
\draw[->] (0,-2) -- (0,2) node[anchor=east] {$V_{L1}$};
\draw[thick,dashed] (0,-1) -- (1,-1);

\draw	(0.5,0.7) node[anchor=north] {A1}
		(1.5,-0.3) node[anchor=north] {A2}
		(-0.4,1) node {E}
		(-0.9,-0.9) node {$- \dfrac{E \cdot N1}{N2}$}
		(1.2,0.2) node {\small $t_T$}
		(2.1,0.2) node {\small $t_2$}
		(2.7,0.2) node {\small $\tau$};

\draw[thick,line width = 0.08cm] (0,1) -- (1,1) -- (1,-1) -- (2,-1) -- (2,0) -- (2.5,0) -- (2.5,1);

\end{tikzpicture}

\end{center}

\columnbreak

Para o máximo valor de ciclo de trabalho, consideramos $t_2 = \tau$, assim:

\begin{align*}
E \cdot t_T ~&=~  \dfrac{E \cdot N1}{N2} \cdot (\tau - t_T)\\
\dfrac{t_T}{\tau - t_T} &= \dfrac{N1}{N2} = \dfrac{100}{50}\\
\dfrac{\tau \cdot \delta}{\tau - \tau \cdot \delta} &= \dfrac{\tau \cdot \delta}{\tau \cdot (1 - \delta)} = \dfrac{100}{50} \\
&\therefore\\
\dfrac{\delta}{1-\delta} &= \dfrac{100}{50} ~\longrightarrow~ \delta < 2/3 
\end{align*}

\end{multicols}

\subsection*{c) Determine a mínima tensão de bloqueio que o transistor deve suportar.}

A máxima tensão que o transistor deve suportar será a tensão da fonte somada a tensão refletida do enrolamento de desmagnetização, que ocorre durante o período de desmagnetização do núcleo. Assim,

\begin{align*}
Vmax ~=~ E + \dfrac{E \cdot N1}{N2} ~=~ 200 + \dfrac{200 \cdot 100}{50} ~=~ 600~V.  
\end{align*}

\subsection*{d) Qual o número de espiras do enrolamento de saída, N3, caso se deseje uma tensão de saída de 12 V para um ciclo de trabalho de 50\%?}

Através da Equação \eqref{eq.vo.foward} foi encontrado valor de N3 = 12.

\begin{align}
Vo ~=~ \dfrac{E \cdot \delta \cdot N3}{N1} ~\longrightarrow~ N3 ~=~ \dfrac{Vo \cdot N1}{\delta \cdot E} ~=~ 12
\label{eq.vo.foward}
\end{align}

\end{document}
