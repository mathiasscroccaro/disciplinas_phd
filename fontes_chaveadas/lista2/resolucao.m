clear all;
close all;
clc;
format short eng;

%%% Exercicio 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

E = 10;
L1 = 50e-6;
L2 = 5e-3;
n = 10;
f_chav = 25e3; tau = 1/f_chav;
C1 = 5e-6;
Ro = 100;
delta = 0.5;

printf("Questão 1\n");

% A

Vo = E*n*delta/(1-delta);

printf("a) Tensao de saida calculada: %d\n",Vo);
printf("a) Tensao de saida simulada: %d\n",92);

% B

Io = Vo/Ro

delta_Vo = Io*tau*delta/C1;

printf("b) Ondulacao da tensao de saida calculada: %d\n",delta_Vo);
printf("b) Ondulacao da tensao de saida simulada: %d\n",3.57);

% C

printf("c) A tensao Vds do transistor: Idealmente eh zero\n");
printf("c) Valor de pico encontrado na simulacao: %d\n",100.3);

% D

delta_IL1 = E*tau*delta/L1;
delta_IL2 = delta_IL1/n;

printf("d) Ripple de corrente calculado no indutor primario: %d; secundario: %d\n",delta_IL1,delta_IL2);
printf("d) Ripple de corrente simulado no indutor primario: %d; secundario: %d\n",10.59,1.39);

%%% Exercicio 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

printf("\nQuestão 4\n");
