clear all;
close all;

fs = 1e6;
f = 60;

n = 0:1/fs:100e-3;

v = sin(2*pi*f*n) + 1;
i = sin(2*pi*f*n);

M = csvread('data_4.csv');

t = M(:,1);
v = M(:,2);
i = M(:,3);

t = t(round(end/2):end);
v = v(round(end/2):end);
i = i(round(end/2):end);

p = (1/(t(end)-t(1)))*(1/fs)*trapz(v.*i)

p = mean(v.*i)
s = rms(i) * rms(v)

fp = p/s

plot(t,v.*i);
%hold on;
%plot(n,i);

X = 0:pi/100:pi;
Y = sin(X);
figure;
plot(Y)
Q = trapz(X,Y)