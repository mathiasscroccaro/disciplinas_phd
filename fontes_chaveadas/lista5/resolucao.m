clear all;
close all;

pkg load ltfat;

M = csvread('data_4_teste.csv');

f = M(:,1);
Amp = M(:,2);

points = length(f)/2;

f = f(1:points);
Amp = Amp(1:points);

i_rms = 0;

fund = 60;

for i = 1:100
  [value,idx] = min(abs(f-fund*i));
  printf("Freq. %d - Amplitude %d\n",fund*i,Amp(idx));
  if (i==1)
    i_1 = Amp(idx);
  endif
  i_rms = i_rms + (Amp(idx)^2); 
endfor

i_rms = sqrt(i_rms)
v_rms = 220

i_1
v_1 = 311

p = i_1*v_1*0.5
s = v_rms * i_rms

FP = p/s