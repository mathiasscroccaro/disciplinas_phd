clear all;
close all;

pkg load ltfat;

M = csvread('data_4.csv');

t = M(:,1);
v = M(:,2);
i = M(:,3);

t = t(round(end/2):end);
v = v(round(end/2):end);
i = i(round(end/2):end);

v_rms = rms(v)
i_rms = rms(i)
v_p = v_rms*sqrt(2)

figure;
subplot(211)
plot(t,v);
subplot(212)
plot(t,i);

figure;
plot(t,v/15);
hold on;
plot(t,i);

figure(666);
h = (fft(i));
h = abs(h);
i_1 = h(4)/(length(i)/2)
h = h/(length(i)/2);
h = h(1:end/2);
plot(h);

idx_h = 7:3:length(h);
DHT = sqrt(sum(h(idx_h).*h(idx_h)))/h(4);
DHT

h(idx_h(1:20))
h(4)

%p = v_p*i_1*0.5
p = mean(v.*i)
s = v_rms*i_rms

FP = p/s

cos_phi = i_rms*FP/(i_1/sqrt(2))
cos_phi = FP*sqrt(1+DHT^2)

Po = 229*229/100;
Vp = v_p;
R_em = (v_p^2)/(2*Po)

figure;
plot(t,v.*i)
