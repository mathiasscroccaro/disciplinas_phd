clear all;
close all;

pkg load signal;

delta = 0.2;

V = 1;
I = 1;

t = linspace(0,2*pi,1000);

v_t = square(t);
i_t = square(t-delta*2*pi);

P = 2*(0.5-2*delta)*V*I

i_a = (P/(rms(v_t)^2)).*v_t;

i_na = i_t - i_a;

figure;
subplot(411)
plot(v_t)
subplot(412)
plot(i_t)
subplot(413)
plot(i_a)
subplot(414)
plot(i_na)

t = linspace(0,4*pi,5000);

v_ii = sqrt(2)*sin(t) + 0.1*sqrt(2)*sin(5*t);
i_l = sqrt(2)*cos(t) + sqrt(2)*sin(t) - 0.5*sqrt(2)*sin(5*t);

i_c = -0.5*sqrt(2)*sin(5*t);
i_s = i_l - i_c;

W1 = mean(i_s.*v_ii)

i_c = 1.5*sqrt(2)*sin(5*t);
i_s = i_l - i_c;

W2 = mean(i_s.*v_ii)

W3 = mean(i_l.*v_ii)
