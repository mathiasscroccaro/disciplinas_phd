clear all;
close all;

f = 60;
w = 2*pi*f;

global Vcc = 250;
Vac_rms = 127;
global Vac_pico = Vac_rms*sqrt(2);

L = 10e-3;
R = 50e-3;

global Xs = w*L*i + R;

%Xs_angle = rad2deg(angle(Xs))
%Xs_abs = abs(Xs)

function y = f(x)
  global Xs;
  global Vac_pico;
  global Vcc;
  y = zeros(1,1);
  y(1) = angle((Vcc*(cosd(x(1))+i*sind(x(1))) - Vac_pico)/(Xs));
endfunction

[theta, fval, info] = fsolve (@f, [50]);


Iac_pico = real((250*exp(j*deg2rad(theta)) - 180)/Xs)
Iac_rms = Iac_pico/sqrt(2)
P = Iac_rms*Vac_rms

%n = linspace(0,2*pi,100);

%Ix_sen = abs(Ix)*sin(2*pi*60*n);
%Vac_sen = 180*sin(2*pi*60*n);

%P_sen = Ix_sen.*Vac_sen;
%P_sen = mean(P_sen)

%Pp = 180*Ix

%Pac = (250*exp(j*deg2rad(theta))*127)*sind(theta)/Xs