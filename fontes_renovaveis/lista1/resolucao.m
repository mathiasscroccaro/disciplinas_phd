

clear all;
close all;
clc;
format short eng;

%%% Exercicio 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

E = 48;
Vo = 36;
Ro = 9;
f_chav = 64e3; tau = 1/f_chav;
L1 = 10e-3;
L2 = 1e-3;
Co = 100e-6;

printf("Questão 1\n");

% A

Io = Vo/Ro;

Le = (L1*L2)/(L1 + L2);
Ke = (Le*Io)/(E*tau);

if (Ke > 1/8)
  printf("a) Conversor Cuk em MCC, pois Ke = %d, > 1/8\n",Ke);
endif

% B

delta = (Vo/E)/(1+Vo/E);

printf("b) Ciclo de trabalho: %d\n",delta);

% C

VC1_ripple_critico = 2*(E + Vo);
VC1_ripple_desejado = 0.5;
razao = VC1_ripple_critico/VC1_ripple_desejado;

C1_min = (Io*delta*(1-delta)*tau)/(2*E);
C1 = C1_min * razao;

printf("c) Para ripple de 0,5 Vpp, C1 = %d\n",C1);

% D

Ii = IL1 = Vo*Io/E;
Delta_IL2 = (Vo*(1 - delta)*tau)/(L2);

printf("d) \n\tCorrente da entrada: %d A\n\tVariacao na corrente IL2: %d A\n",Ii,Delta_IL2);

%%% Exercicio 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

E = 100;
Ro = 200;
f_chav = 10e3; tau = 1/f_chav;
L = 1e-3;
Co = 47e-6;
delta = 0.5;

printf("\nQuestão 2\n");

% A

function y = f(x)
  E = 100;
  Ro = 200;
  f_chav = 10e3; tau = 1/f_chav;
  L = 1e-3;
  Co = 47e-6;
  delta = 0.5;
  
  y = zeros(1,1);
  y = 1 + (delta^2)/(2*((L*x/Ro)/(E*tau))) - x/E;
endfunction

[Vo, fval, info] = fsolve(@f,[1]);

Io = Vo/Ro;

K = (L*Io)/(E*tau);

if (K > 1/8)
  printf("a) Conversor Boost em MCC, pois K = %d, > 1/8\n",K);
else
  delta_crit_1 = (1+sqrt(1-8*K))/2;
  delta_crit_2 = (1-sqrt(1-8*K))/2;
  
  if (delta > delta_crit_1 || delta < delta_crit_2)
    printf("a) Conversor Boost em MCC, pois K = %d, < 1/8, e (delta > delta_crit ou delta < delta_crit)\n",K);
  else
    printf("a) Conversor Boost em MCD, pois K = %d, < 1/8, e (delta_crit < delta < delta_crit)\n",K)
  endif
endif

% B

printf("b) Tensao media da saida: %d\n",Vo);

% C

Delta_IL = (E*(tau*delta))/L;

printf("c) Ondulação na corrente do indutor: %d A\n",Delta_IL);

% D

function y = g(x)
  E = 100;
  f_chav = 10e3; tau = 1/f_chav;
  delta = 0.5;
  Vo = 215.8;
  
  y = zeros(1,1);
  y = (1 - x/tau)/(1 - delta - x/tau) - Vo/E;
endfunction

[tx, fval, info] = fsolve(@g,[10e-6]);

printf("d) Valor de tx: %d s\n",tx);

%%% Exercicio 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

delta = 0.5;
f_chav = 25e3; tau = 1/f_chav;
Vo = 10;
E = 20;

printf("\nQuestão 3\n");

% A

L = (E*delta*(1-delta)*tau)/(2*1);

printf("a) O valor do indutor é de %d H\n",L);

% B

Co = (Vo*(1-delta)*tau^2)/(8*L*0.01*Vo);

printf("B) O valor do capacitor é de %d F\n",Co);

% D

K = (L*0.5)/(E*tau)
Vo = E*(delta^2)/(delta^2 + 2*K)

printf("d) Valor de Vo em MCD: %d V\n",Vo);

% E

Ro = Vo/0.5










