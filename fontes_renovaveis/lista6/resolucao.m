clear all;
close all;

pkg load signal

format short eng

% letra (a)

f = 60;
Lf = 0.02;
Lg = 0.001;

Xlf = 2*pi*f*Lf
Xlg = 2*pi*f*Lg

Vpac_rms = 100e3;

Vw_rms = 200e6*Xlf/100e3
Vw_p = Vw_rms * sqrt(2)

%theta = asind(200e6*Xlg/(100e3*100e3))

% letra (b)

Vg_h_11 = 1e3;

Zf = 0.1 + Xlf*i;
Zg = 0.1 + Xlg*i;

Vpac_h_11 = Vg_h_11*(Zf)/(Zf+Zg)
Vpac_h_11_rms = rms(Vpac_h_11)

DHT = sqrt(Vpac_h_11_rms^2/Vpac_rms^2)

% letra (c)

L = 0.002;

% Xs = sL + 1/sC = (s^2 CL + 1)/sC
% para que Xs -> 0, s^2 CL + 1 = 0. Assim:
C = 1/((2*pi*f*11)^2 * L)

% letra (f)

Vg_rms = sqrt((100e3)^2+(1e3)^2)
% Como são cossenoides, é possivel concluir que
Vg_rms_chapeu = Vg_rms;

%W = 

%Ir = W*/(Vg_rms_chapeu^2)

%Q = Vg_rms*Ir

Vpac = 141.4e3;

w = 2*pi*60;
Xjw = ((i*w)^2 * C * L + 1)/(i * w * C) + 1;

ipac_t = rms(Vpac/Xjw)
Ipac_angle = rad2deg(angle(Vpac/Xjw))

t = linspace(0,4*pi,5000);

v_t = 141.3e3 .* cos(t);
V = 100e3;

v_chapeu_t = 141.4e3 .* sin(t);
V_chapeu = 100e3;

i_t = 141.4e3/(abs(Xjw))*cos(t+angle(Xjw));

Wr = mean(v_chapeu_t.*i_t)

ir_t = (Wr/(V_chapeu^2)).*v_chapeu_t;
Ir = rms(ir_t)
Q = V*Ir

ig_t = i_t + 1.41e3 .* cos(11*t) + 2.82e3.*cos(t);
Ig = rms(ig_t)

S = sqrt(Q^2 + (200e6)^2)

figure(1); plot(v_t); title("v_{PAC}(t)")
figure(2); plot(v_chapeu_t); title("v_{PAC}_{chapeu}(t)")
figure(3); plot(i_t); title("i(t)")
figure(4); plot(ir_t); title("i_r(t)")
figure(5); plot(ig_t); title("i_g(t)")


v1 = 21.3e3 * (cos(pi/2) + i*sin(pi/2));
v2 = 141.4e3 * (cos(0) + i*sin(0));

i1 = (v1-v2)/Zf
I1 = abs(i1)/sqrt(2)

I11 = 1e3;

I = sqrt(I1^2 + I11^2)


