%ModeratorVerifiedStreamElements
%: Para contribuir! PICPAY - @djpg1977 👽 PIX/PAYPAL - djpg1977@gmail.com

% RESOLUÇÃO DA LISTA 2
% IT744 - Eletronica de Potencia para GTD

close all;
clear all;

format short eng;

pkg load signal

% Exercício 1

Vrms_fase = 115; Vp_fase = Vrms_fase*sqrt(2); Vp_linha = Vp_fase * sqrt(3); Vrms_linha = Vp_linha / sqrt(2);

alpha_deg = 30; alpha_rad = alpha_deg*pi/180;

Ro = 4;

% a) Tensão média de saída
Vo_rms = 3*sqrt(2)*Vrms_linha*cos(alpha_rad)/pi; Io_rms = Vo_rms/Ro
printf("1.a) Vo = %f\n",Vo_rms);

% b) Potência na carga
Po = Io_rms*Vo_rms;
printf("1.b) Po = %f\n",Po);

% c) Fator de potência
FP = 3*cos(alpha_rad)/pi;
printf("1.c) FP = %f\n",FP);

% d) Fator de deslocamento da fundamental
FD = cos(alpha_rad);
printf("1.d) FD = %f\n",FD);

% e) Fator de forma da corrente
Ii1 = 0.78*Io_rms; FFC = Ii1/Io_rms;
printf("1.e) FFC = %f\n",FFC);

% f) Distorção Harmônica Total da corrente
printf("1.f) DHT = 31.08\n",FFC);

% Exercício 4

V1 = 100; V5 = 0.04*V1; V7 = 0.03*V1;
I1 = 50; I5 = 0.11*I1; I7 = 0.07*I1;
Pef = 4010; P = 4000;

% a) Tensão e corrente eficazes totais: Vef, Ief; e potência aparente S
Vef = sqrt(V1^2 + V5^2 + V7^2);
Ief = sqrt(I1^2 + I5^2 + I7^2);
Pef = Vef*Ief;
printf("\n");
printf("4.a) Vef = %f\tIef = %f\tPef = %f\n",Vef,Ief,Pef);

% b) Distorção harmônica total de tensão e corrente: DHTv, DHTI;
DHTv = sqrt(V5^2 + V7^2)/V1;
DHTi = sqrt(I5^2 + I7^2)/I1;
printf("4.b) DHTv = %f\tDHTi = %f\n",DHTv,DHTi);

% c) Fator de potência FP;
%FP = I1*cosd(alpha)/Ief;
FP = P/(Pef);
printf("4.c) FP = %f\n",FP);

% d) Fator de deslocamento da fundamental.
cos_alpha = P/(V1*I1);
alpha = acosd(cos_alpha);
printf("4.d) alpha = %f\n",alpha)