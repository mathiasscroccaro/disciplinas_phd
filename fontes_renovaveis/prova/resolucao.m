clear all;
close all;

f = 60;

Lf = 1e-3;
Cf = 150e-6;
Rf = 0.1;

Ls = 380e-6;
Rs = 0.1;

h = 7;

Xf = Rf + 2*pi*h*f*Lf*i + inv(2*pi*h*f*Cf*i);
Xs = Rs + 2*pi*f*h*Ls*i;

I = 15;

Irede_carga = abs(I * (Xf/(Xf+Xs)))

Vrede7 = 0.03 * 220;

Irede7 = abs(Vrede7/(Xf+Xs))

h = 1;

Xf = Rf + 2*pi*h*f*Lf*i + inv(2*pi*h*f*Cf*i);
Xs = Rs + 2*pi*f*h*Ls*i;

Irede = abs(220/(Xf+Xs));

DHTrede = sqrt( (Irede7^2)/(Irede^2) )

clear all;
close all;

Rdc = 1.3;
Udc2 = 96e3;
Pref2 = 186e6;

Idc = Pref2/Udc2;

Udc1 = Udc2 + Idc*Rdc