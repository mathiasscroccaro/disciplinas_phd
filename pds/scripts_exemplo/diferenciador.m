%Exemplo de processamento digital de sinais
close all; clear all;
%Coleto amostras de um sinal x(t) e, por meio de um sistema a tempo
%discreto, obtenho amostras da derivada de x(t).

%x(t) = sin(2*pi*f*t)

%freq. da senóide
f = 5;
%freq. de amostragem
fs = 20; ts = 1/fs;
%número de amostras (para cada lado)
N = 20;
%sinal amostrado
x = sin(2*pi*f/fs*(-(N-1):1:N-1));

%eixo do tempo
t = -2*(N-1)*ts:1e-4:2*(N-1)*ts;

figure; stem(-(N-1):1:N-1,x); xlabel('n'); ylabel('x(n)');

%filtro diferenciador (versão aproximada - janelada)
n = -(N-1):1:N-1;
%resposta ao impulso (truncada) do sistema diferenciador
h = fs*cos(pi*n)./n; h(N) = 0;

%resposta em freq.
fh = fft(h,2048);
wk = (2*pi/2048)*[0:1:2047]; %(0 a N-1 para colocar na posição certa)
%espectro de amplitudes (magnitudes)
figure; stem(wk,abs(fh)); xlabel('Freq. \omega (rad)'); ylabel('|H(k)|');

%determina a saída do sistema diferenciador
y = conv(x,h);

figure; plot(-2*(N-1):1:2*(N-1),y,'b'); hold on;
xx = 2*pi*f*cos(2*pi*f/fs*(-2*(N-1):1:2*(N-1))); plot(-2*(N-1):1:2*(N-1),xx,'r');
ylabel('Amplitude'); xlabel('n'); legend('y(n)','amostras da derivada');


for n = 0:1:length(y)-1,
    %func = sin(omegas./2*(t-n*ts))./(omegas/2*(t-n.*ts));
    %corrige os casos em que há indefinição no cálculo da função sinc
    %func(isnan(func)) = 1;
    %st(n+1,:) = xn(n+1)*func;
    func2 = sinc(1/ts*(t-n*ts));
    st2(n+1,:) = y(n+1)*func2;
end
figure; hold on; plot(t,sum(st2),'k');

%OBSERVAÇÕES:

%olhar o espectro (FFT) do filtro - ele é uma aproximação apenas da reta
%crescente de 0-pi, e decrescente de pi-2*pi, já que fizemos o truncamento
%da resposta ao impulso

% - como os dois sinais foram truncados, obtemos amostras corretas da
% derivada do sinal em um intervalo finito...nas pontas, as amostras não
% correspondem ao valor correto.

% - uma forma de mitigar este problema seria prosseguir com o janelamento e
% efetuar um overlap and save (ou overlap and add)

