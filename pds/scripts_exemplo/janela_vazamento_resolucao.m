%Vazamento em frequência, resolução (que depende da janela) e variações da janela
close all; clear all;

%Sinal a tempo contínuo: x(t) = A0*cos(Omega_0 *t + theta_0) + A1*cos(Omega_1*t + theta_1)
%Este sinal possui duas raias bem claras, uma em Omega_0 e outra em Omega_1

%Amostragem:
%x(n) = A0*cos(w0*n + theta0) + A1*cos(w1*n + theta1), com w0 = Omega_0*Ts e w1 = Omega_1*Ts

%Janelamento:
%v(n) = x(n)*w(n)
%V(e^jw) =  A0/2*e^(j*theta0)*W(e^{j(w - w0)}) +
%         + A1/2*e^(j*theta1)*W(e^{j(w - w1)})
%         + A0/2*e^(-j*theta0)*W(e^{j(w + w0)}) +
%         + A1/2*e^(-j*theta1)*W(e^{j(w + w1)})


%PARTE 1 - VENDO AS PROPRIEDADES DAS JANELAS

%modelo geral: janela de Kaiser

%largura do lóbulo principal - fator determinante para a resolução em
%frequência (conseguir distinguir com clareza frequências cada vez mais
%próximas)

%amplitude relativa do lóbulo secundário em relação ao primário - fator que
%evidencia o quanto as demais freqs. (mais afastadas) influenciam na observação de uma certa freq.

%Exemplo - retangular: lóbulo principal mais estreito = melhor resolução
%porém, relação de amplitude com o lóbulo secundário (e demais lóbulos) não
%é boa, de modo que várias freqs. distantes influenciam no valor observado do espectro numa dada frequência.

%comprimento da janela (N) - para ter valores de 0 a N-1 - influencia a largura do lóbulo principal (qto maior, menor o lóbulo).
N = 21; 
%parâmetro beta - influencia a amplitude relativa do lóbulo secundário
beta = 6;
%obtém os coeficientes da janela de Kaiser
ks=kaiser(N,beta);
%visualização da janela de Kaiser e de sua resposta em freq.
figure; stem(0:N-1,ks); xlabel('n'); ylabel('w(n)');
figure; [Xejw,w] = freqz(ks,1); plot(w,abs(Xejw)); ylabel('|W(e^{j\omega})|'); xlabel('\omega (rad/s)');
figure; plot(w,20*log10(abs(Xejw)./abs(Xejw(1)))); ylabel('|W(e^{j\omega})| (dB)'); xlabel('\omega (rad/s)'); ylim([-100 0]);

%PARTE 2 - FREQS. DO SINAL CASANDO (OU NÃO) COM FREQS. AMOSTRADAS NA DFT

%sinal amostrado e truncado com a janela retangular de comprimento N = 64

%parâmetros da sequência
N = 64; omega0 = 2*pi/14; omega1 = 4*pi/15; A0 = 1; A1 = 0.75;
%N = 1024; omega0 = 2*pi/10; omega1 = 2*pi/10+0.02; A0 = 1; A1 = 0.75;
%janela retangular
beta = 0;
%número de zeros - zero-padding
Nz = 0;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);

pause;

%OBSERVAÇÕES:
%  - o pico real da transf. de Fourier não é identificado pela DFT (não há
%  uma amostra tal que 2*pi/N*k seja igual a 2*pi/14 ou 4*pi/15
%  - as localizações dos picos da DFT não necessariamente coincidem com as
%  frequências exatas dos picos da transf. de Fourier

%parâmetros da sequência
N = 64; omega0 = 2*pi/16; omega1 = 2*pi/8; A0 = 1; A1 = 0.75;
%janela retangular
beta = 0;
%número de zeros - zero-padding
Nz = 0;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);

pause;

%Ao fazer o zero-padding de v(n), para que ela tenha 128 amostras
Nz = 64;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);

pause;

%Com o zero-padding, voltamos a enxergar amostras do conteúdo em freq. (que
%não se restringe às freqs. verdadeiras da senóide por causa do vazamento).
%IMPORTANTE: zero-padding nada pode fazer contra vazamento - ele não afeta
%a resolução do espectro (de V(E^jw)).

%PARTE 3 - USANDO A JANELA DE KAISER, EM LUGAR DA JANELA RETANGULAR

%Vamos retornar ao caso em que não há casamento perfeito de 2*pi/N*k com as
%freqs. verdadeiras das cossenóides

%parâmetros da sequência
N = 64; omega0 = 2*pi/14; omega1 = 4*pi/15; A0 = 1; A1 = 0.75;
%janela de Kaiser - garante amplitude relativa do lóbulo secundário igual a 40dB
beta = 5.48;
%número de zeros - zero-padding
Nz = 0;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);

%O que se observa:
% - há uma separação em dois picos. Por conta da maior redução dos lóbulos
% secundários, não percebemos tantos outros pontos com valores "não-nulos"
% fora dos picos
% - o lóbulo principal é mais largo, mas ainda dá conta de separar as
% freqs.

pause;

%SE EU USAR UMA JANELA DE KAISER MENOR, A LARGURA DO LÓBULO PRINCIPAL
%AUMENTA, O QUE PODE COMPROMETER A SEPARAÇÃO DOS PICOS

%parâmetros da sequência
N = 32; omega0 = 2*pi/14; omega1 = 4*pi/15; A0 = 1; A1 = 0.75;
%janela de Kaiser - garante amplitude relativa do lóbulo secundário igual a 40dB
beta = 5.48;
%número de zeros - zero-padding
Nz = 0;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);

pause;

%PARTE 4 - FAZER O ZERO-PADDING NA HORA DE CALCULAR A DFT

%Usando uma janela de Kaiser de comprimento N = 32, vimos que não
%conseguimos atingir uma separação dos picos adequada
%Ao manter a seq. com comprimento N = 32, mas inserindo zeros nela para
%calcular uma DFT com, por exemplo, 128 pontos, o que se observa é um
%número maior de amostras (e uma separação menor entre as amostras). Porém,
%não melhoramos em nada a questão de resolução das freqs., que depende
%inteiramente da janela (no caso, do lóbulo principal dela).

%parâmetros da sequência
N = 32; omega0 = 2*pi/14; omega1 = 4*pi/15; A0 = 1; A1 = 0.75;
%janela de Kaiser - garante amplitude relativa do lóbulo secundário igual a 40dB
beta = 5.48; 
%número de zeros para o zero-padding
Nz = 96;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz)

%USANDO ZERO-PADDING E UMA JANELA MELHOR, GANHAMOS NOS DOIS ASPECTOS

pause;

%parâmetros da sequência
N = 64; omega0 = 2*pi/14; omega1 = 4*pi/15; A0 = 1; A1 = 0.75;
%janela de Kaiser - garante amplitude relativa do lóbulo secundário igual a 40dB
beta = 5.48;
%número de zeros para o zero-padding
Nz = 64;
%exibe as respostas em freq. e a DFT
mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz);


