%Parâmetros de entrada
%N - comprimento da sequência
%omega0 - freq. da primeira componente senoidal
%omega1 - freq. da segunda componente senoidal
%A0 - amplitude da primeira componente senoidal
%A1 - amplitude da segunda componente senoidal
%beta - parâmetro da janela de Kaiser (e.g., 5.48 garante uma amplitude relativa do lóbulo secundário igual a 40dB)
%Nz - número de zeros acrescentados (zero-padding)


function mostra_seq_janela_DFT(N,omega0,omega1,A0,A1,beta,Nz)

%índices da sequência
n=0:N-1; 
%seq. original
vn = A0*cos(omega0*n)+A1*cos(omega1*n);
%cria a janela de Kaiser
ks = kaiser(N,beta)';

%sequência janelada
vn = vn.*ks;

%resposta em frequência da sequência janelada
[Hejw,w]=freqz(vn,1,4096); figure; plot(w,abs(Hejw),'k'); hold on;

%Cálculo da DFT com zero-padding
vk = fft([vn zeros(1,Nz+N)],N+Nz);
%freqs. digitais correspondentes aos índices de 0 a (Nz+N)-1
wk = (2*pi/(Nz+N))*(0:1:(Nz+N)-1); 
%plota as amostras da transformada de Fourier
stem(wk,abs(vk),'.'); 
%limitamos a visualização ao intervalo de 0 a pi (de pi a 2*pi é uma
%repetição, já que o sinal é real)
xlim([0 pi]); ylabel('|V(e^{j\omega})|'); xlabel('\omega (rad)');

