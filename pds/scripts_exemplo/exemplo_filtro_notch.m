%Filtro Notch (Exemplo)

close all;

num = [1 -1];
den = [1 -0.95];
%resposta em frequência
[H,w] = freqz(num,den,2048);

figure; plot(w,abs(H));
xlabel('\omega (rad/s)');
ylabel('|H(e^{j\omega})|');
grid;
xlim([0 pi/2]);
