%Vazamento em frequência da DFT - Leakage
close all; clear all;
%amostragem de uma senóide de freq. f a uma taxa de fs
f0 = 1;
%tempo máximo
tmax = 1; 
%número de amostras desejadas
N = 10;
%taxa de amostragem
fs = N/tmax;
%desejamos uma sequência x(n) contendo N amostras (e.g., 64) no intervalo de 0 a 1s
xn = cos(2*pi*f0/fs*[0:1:N-1]);

%de posse da sequência, desejamos observar sua DFT

%número de pontos da DFT (se for maior que N, temos zero-padding)
Na = 10;
xk = fft(xn,Na);
wk = (2*pi/Na)*[0:1:Na-1]; %(0 a N-1 para colocar na posição certa)

figure; stem(0:N-1,xn); xlabel('n'); ylabel('x(n)');
figure; stem(wk,abs(xk)); xlabel('\omega (rad)'); ylabel('|X(k)|');hold on; 
Xejw = fft(xn,4096);
wkj = (2*pi/4096)*[0:1:4096-1]; %(0 a N-1 para colocar na posição certa)
plot(wkj,abs(Xejw)); legend('DFT','Trans. Fourier da seq. janelada');
figure; stem(0:Na-1,abs(xk)); xlabel('k'); ylabel('|X(k)|');



