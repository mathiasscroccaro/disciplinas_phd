%Análise da resposta em frequência
%Sistema com 2 pólos: em z = 0 e em z real
%        com 2 zeros: em (1/2) +- j (1/2)

close all;

num = [1 -1 0.5];

for polo = -0.9:0.1:0.9
    

    den = [1 -polo];
    %resposta em frequência
    [H,w] = freqz(num,den,2048);
    figure; plot(w,abs(H));
    xlabel('\omega (rad/s)');
    ylabel('|H(e^{j\omega})|');
    grid;
    title(['Módulo da resposta em frequência - Polo = ' num2str(polo)]);
    %xlim([0 pi/2]);
    
end