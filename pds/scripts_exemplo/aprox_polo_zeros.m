%Aproximação de um pólo usando N zeros
clear all; close all;
pkg load signal

%sistema simples com um único pólo

%H(z) = z / (z - a)
%H(e^jw) = 1 / (1 - a*e^{-jw})

%pólo
a = -0.9;
%computa o valor de H(e^jw)
[h,w] = freqz(1,[1 -a]);

%N - 1 zeros uniformemente espalhados na circunferência de raio a
for kk = 2:1:20
    k = 1:kk-1;
    %zeros da aproximação de H(z)
    zer = a*exp(1i*2*pi/kk*k)';
    %monta a função de transferência
    [num,den] = zp2tf(zer,[],1);
    [haprox,wa]=freqz(num,den);
    figure; plot(w,abs(h),'r'); xlabel('\omega (rad/amostra)'); ylabel('|H(e^{j\omega}|'); grid;
    hold on; plot(wa,abs(haprox),'k'); legend('Pólo',[num2str(kk) ' zeros']);
end

%Observações:
%se a é pequeno (próximo da origem), é bem fácil simular o sistema usando poucos zeros
%à medida que a se aproxima da CRU, preciso cada vez mais de zeros para que a aproximação seja boa