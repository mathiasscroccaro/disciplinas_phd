%Amostragem de um sinal contínuo e sua recuperação a partir das amostras
%conforme os esquemas: ideal (FPB)

close all; clear all;

%sinal original - tipo sinc, com freq. máxima igual a W (Hz)

%tempo de observação máximo
tmax = 1;
%eixo do tempo ("contínuo")
t = 0:1e-4:tmax;

%se o intervalo da função começar no negativo, temos que corrigir o índice
%n, para manter a consistência que n = 0 fica associado ao instante t = 0,
%n é negativo para t negativos, etc. sem isto, temos uma versão deslocada
%do sinal recuperado.

%freq. máxima do espectro (no caso, um retângulo)
W = 80; 
%sinal analógico
xt = sin(W*t)./(pi*t);

%por Nyquist, precisamos de 2*W amostras / s - fs

%acréscimo em relação à mínima freq. de amostragem
deltaW = 150; 
fs = 2*W + deltaW; ts = 1/fs; omegas = 2*pi*fs;

%amostrando o sinal com fs
tamost=0:1/fs:tmax;
xn = sin(W*tamost)./(pi*tamost); xn(isnan(xn)) = W/pi;

%reconstruindo o sinal através de FPB ideal
figure;
%exibe as funções base ponderadas que serão somadas (no caso, sincs)
for n = 0:1:length(xn)-1,
    plot(t,sinc(1/ts*(t-n*ts))); hold on;
end

for n = 0:1:length(xn)-1,
    %func = sin(omegas./2*(t-n*ts))./(omegas/2*(t-n.*ts));
    %corrige os casos em que há indefinição no cálculo da função sinc
    %func(isnan(func)) = 1;
    %st(n+1,:) = xn(n+1)*func;
    func2 = sinc(1/ts*(t-n*ts));
    st2(n+1,:) = xn(n+1)*func2;
end
figure; hold on; plot(t,xt,'--','Color',[0.5 0.5 0.5]); stem(tamost,xn,'r');  plot(t,sum(st2),'k');
%figure; plot(t,sum(st2),'r');

%O que aconteceu?
%O sinal que conseguimos amostrar não foi o sinc(), mas sim sinc()*janela
%retangular, pois limitamos a observação ao intervalo de 0 a 1 segundos.
%Neste caso, o espectro, embora guarde relações com o retângulo esperado,
%se alonga e passa a ter duração muito maior que o anterior.

%Por isso, mesmo usando uma freq. maior que a taxa de Nyquist para a
%amostragem do sinal, não conseguimos recuperá-lo perfeitamente. 

%Outra desvantagem: preciso conhecer todas as amostras e usar todas para
%estimar o valor de x(t) em qualquer instante de tempo. Outras formas de
%interpolação, por exemplo, requerem o conhecimento de 1 ou 2 amostras
%somente (em geral, nos instante nTs e (n+1)Ts).
