clear all;
close all;

n0 = 3;

y = []
x = [1 zeros(1,60)];

for n = 0:60;

    if n<n0
       y = [y (x(n+1))];
    else
        y = [y (x(n+1)+0.25*y(n-n0+1))];
    endif
    
endfor

w = y;
W = fft(w,1024);

figure;
stem(0:60,w)
title(sprintf("w[n] --- n0 = %d",n0));

h = [1 zeros(1,n0-1) -0.25];
H = fft(h);

G_ = 1./H;
g1 = abs(ifft(G_));

g2 = 0.25.^(0:3);

figure;w_ = linspace(0,pi,1024); plot(w_,abs(W)); title('W(k) e G(k)'); hold on;
w_ = linspace(0,pi,1024); plot(w_,abs(fft(g2,1024))); 

x = cos((0:100).*pi/10) + 0.5*cos((0:100).*pi/20);

r = conv(x,h);

y1 = conv(w,r);
y3 = conv(g1,r);
y2 = conv(g2,r);

figure;
subplot(4,1,1)
plot(x); title('entrada x[n]');
subplot(4,1,2)
plot(y1); title('saida final y[n] = x[n]*h[n]*w[n]');
subplot(4,1,3)
plot(y2); title('saida final y[n] = x[n]*h[n]*g1[n] ----- g1[n] = ifft(1./H)');
subplot(4,1,4)
plot(y3); title('saida final y[n] = x[n]*h[n]*g2[n] ----- g2[n] = (0.25)^n');

figure;
subplot(3,1,1)
stem(conv(h,w)); title('h[n]*w[n]');
subplot(3,1,2)
stem(conv(g1,h)); title('h[n]*g1[n] ----- g1[n] = ifft(1./H)');
subplot(3,1,3)
stem(conv(g2,h)); title('h[n]*g2[n] ----- g2[n] = (0.25)^n');

g = [1 zeros(1,n0-1)];
g = [g g g g];
n = 0:(length(g)-1);
g = g.*(0.25.^(n/n0))./(1-0.25^4)
figure;plot(abs(fft(g,1024))); hold on; plot(abs(fft(w,1024)))
