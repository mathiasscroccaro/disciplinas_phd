clear all;
close all;

n0 = 1;

h = [1 -0.25];

g_num = [1 0];
g_den = [1 -0.25];

x = [1:10]

r = conv(x,h)

y = filter(g_num,g_den,r)

figure;
plot(x);
title('x[n]');

figure;
plot(r);
title('r[n]');

figure;
plot(y);
title('y[n]');

figure;
[H1,w] = freqz(g_num,g_den,1000);
plot(w,abs(H1))
title('freqz g')

figure;
[H2,w] = freqz(h,[1],1000);
plot(w,abs(H2))
title('freqz h')

