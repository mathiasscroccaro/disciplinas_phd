close all
n = 10; % comprimento da janela

index = 0:1:n-1

seq = cos(((2*pi)/10)*index);
% seq = cos((pi/7)*index)+ (0.75*cos(index*4*pi/15))
N = 10;
wk = (2*pi/N)*[0:1:N-1]; %(0 a N-1 para colocar na posição certa)
dft_seq = fft(seq,N);
stem(wk,abs(dft_seq))

hold

N = 10000;
wk = (2*pi/N)*[0:1:N-1]; 
dft_seq = fft(seq,N);
plot(wk,abs(dft_seq),'k')