%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Rotina para comparacao entre interpolacao classica e polifasica
%
%   Autor: Mathias S. Costa
%   Data: 08/05/2020 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function E = interpolacao_210322(x,h,L)

figure;
stem(h);
title('Resposta ao impulso do filtro h[n]');
print('imagens/impulso_filtro_interpolacao.png')

%%% INTERPOLACAO POLIFASICA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculando k-enesimos filtros   
figure;
E = [];
for k = 1:L
    % Obtendo os h_k[n] filtros
    temp = h(k:L:end);
    % Preenchendo com zeros h_k[n], se necessário
    % (Octave não trabalha com matrizes de numero irregular de colunas)
    temp = [temp zeros(1,ceil(length(h)/L)-length(temp))];
    
    % Completando a matriz de vetores h_k[n]
    E = [E; temp];
 
    % Plotando os k-enesimos filtros h_k[n]
    subplot(L,1,k);
    stem(temp); 
    title (sprintf ("h_{%d}[n]", k-1));
endfor  
print('imagens/k-enesimo_filtro_interpolacao.png')

% Filtro polifasico seguido do expansor
y = zeros(1,L*length(x));
for k = 1:L
    % A entrada é convoluida com os k-enesimos filtros
    temp = conv(E(k,:),x);
    % Deslocamento e tambem expansao    
    y_poli(k:L:length(temp)*L) = temp;
endfor

%%% INTERPOLACAO CLASSICA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_clas = zeros(1,length(x)*L);
y_clas(1:L:end) = x;
y_clas = conv(y_clas,h);

%%% COMPARACAO ENTRE SAIDAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
subplot(2,1,1)
stem(y_poli);
title('y[n] - Interpolacao polifasica')
subplot(2,1,2)
stem(y_clas);
title('y[n] - Interpolacao classica')
print('imagens/comparativo_interpolacao.png')

espectro(y_poli)
title(sprintf('Espectro da interpolacao polifasica L=%d',L));
espectro(y_clas)
title(sprintf('Espectro da interpolacao polifasica L=%d',L));