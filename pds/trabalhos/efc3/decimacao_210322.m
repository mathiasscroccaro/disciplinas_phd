%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Rotina para comparacao entre decimacao classica e polifasica
%
%   Autor: Mathias S. Costa
%   Data: 08/05/2020 (perto do apocalipse)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function E = decimacao_210322(x,h,M)

figure;
stem(h);
title('Resposta ao impulso do filtro h[n]');
print('imagens/impulso_filtro_decimacao.png')

%%% DECIMACAO POLIFASICA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculando k-enesimos filtros   
figure;
E = [];
for k = 1:M
    % Obtendo os h_k[n] filtros
    temp = h(k:M:end);
    % Preenchendo com zeros h_k[n], se necessário
    % (Octave não trabalha com matrizes de numero irregular de colunas)
    temp = [temp zeros(1,ceil(length(h)/M)-length(temp))];
    
    % Completando a matriz de vetores h_k[n]
    E = [E; temp];
 
    % Plotando os k-enesimos filtros h_k[n]
    subplot(M,1,k);
    stem(temp); 
    title (sprintf ("h_{%d}[n]", k-1));
endfor  
print('imagens/k-enesimo_filtro_decimacao.png')

% Compressao seguida do filtro polifasico
% x tem q ser de tamanho multiplo de M,+1
x = [x zeros(1, M + 1 - mod(length(x),M) )];
y = [];
for k = 1:M
    % Obtendo os x[n] deslocados e comprimidos    
    if (k==1)
        temp = x(1:M:end);
    else
        temp = [0 x((-k+2+M):M:end)];
    endif
    % Convoluindo x[n] deslocados e comprimidos com os k-enesimos filtros
    y = [y; conv(temp,E(k,:))];
endfor
% Somando os elementos, obtendo a saida y[n]
y_poli = sum(y);

%%% DECIMACAO CLASSICA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_clas = conv(x,h);
y_clas = y_clas(1:M:end);

%%% COMPARACAO ENTRE SAIDAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
subplot(2,1,1)
stem(y_poli);
title('y[n] - Decimacao polifasica')
subplot(2,1,2)
stem(y_clas);
title('y[n] - Decimacao classica')
print('imagens/comparativo_decimacao.png')

espectro(y_poli)
title(sprintf('Espectro da decimacao polifasica M=%d',M));
espectro(y_clas)
title(sprintf('Espectro da decimacao classica M=%d',M));