%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Programa exemplo para decimacao e interpolacao polifasica
%
%   Autor: Mathias S. Costa
%   Data: 08/05/2020 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

mkdir imagens;

% Sinal de entrada x[n]
n = 0:20;
x = cos(n*pi/8);
espectro(x);
title('Espectro de x[n]');
print('imagens/espectro_entrada.png');

% Fatores de decimacao e interpolacao
M = 5;
L = 3;

% Emprego da rotina de decimacao
h1 = filtro(pi/M-0.1,pi/M);
E1 = decimacao_210322(x,h1,M);

% Emprego da rotina de interpolacao
h2 = filtro(pi/L-0.1,pi/L);
E2 = interpolacao_210322(x,h2,L);