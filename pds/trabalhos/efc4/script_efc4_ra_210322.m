%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Programa exemplo para overlap add e overlap save
%
%   Autor: Mathias S. Costa
%   Data: 07/06/2020 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pkg load signal

clear all;
close all;

mkdir imagens;

% Sinais de entrada x[n] e h[n]
x = 1:8;
h = [1 1];

P = length(h);
M = length(x);

% Plotando dados da entrada h[n] e x[n]
figure(11); 
subplot(2,1,1); stem(x); title('Sinal de entrada x[n]');
subplot(2,1,2); stem(h); title('Filtro h[n]');
print('imagens/sinais_entrada.png')

%%% (a) Computando convolucao linear %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
y = conv(x,h)
figure(1); stem(y); title('(a) Convolucao linear');
print('imagens/a.png')

%%% (b) Obtendo saida atraves da DFT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for N = [8,16,32]
    
    % Computando DFT da resposta ao impulso h[n] e entrada x[n]
    H = fft(h,N);
    X = fft(x,N);

    % Multiplicando DFTs e computando DFT inversa
    Y = H.*X;
    y = abs(ifft(Y));

    % Plotando resultados das DFTs para diferentes pontos
    figure; stem(y); title(sprintf('(b) Convolucao atraves de DFTs - N = %d',N));
    print(sprintf('imagens/b%d.png',N))
endfor

%%% (c) Implementacao do Overlap Add %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tamanho do comprimento da subsequencia
L = 3;
% Matriz com os valores das sequencias x_i[n]
x_m = [];
% Matriz com os valores das sequencias y_i[n] = x_i[n]*h[n]
y_m = [];
% Saida do sistema
y = [];
% DFT(h[n])
H = fft(h,P+M-1);

for n = 1:L:M
    if (n+L-1 >= M)
        x_i = [x(n:end) zeros(1,L-length(x(n:end)))];
    else
        x_i = x(n:(n+L-1));
    endif
    
    x_m = [x_m;x_i];
    
    %y_i = conv(x_i,h);
    y_i = abs(ifft(fft(x_i,P+M-1).*H));
    y_m = [y_m;y_i];
    
    y = [y zeros(1,length(y_i))];
    y(n:(n+length(y_i)-1)) = y(n:(n+length(y_i)-1)) + y_i;
endfor
y = y(1:M+P-1);
% Saida atraves do overlap add
y_add = y

% Apresentando o resultado da saida final
figure(5); stem(y_add); title('(c) Convolucao atraves de Overlap Add');
print('imagens/c.png')

% Apresentados os dados das operacoes de Overlap Add
y_mm = zeros(size(y_m)(1),length(y));
i = 1;
for n = 1:L:M
    y_mm(i,n:(n+length(y_m(i,:))-1)) = y_m(i,:);
    
    figure(6);
    subplot(ceil(M/L),1,i)
    stem(x_m(i,:));
    title (sprintf ("x_{%d}[n]", i));
    
    figure(7);
    subplot(ceil(M/L),1,i)
    stem(y_mm(i,:));
    axis([1 size(y_mm)(2)])
    title (sprintf ("y_{%d}[n]", n));
    hold on;
    i = i+1;
endfor
figure(6);print('imagens/cx.png')
figure(7);print('imagens/cy.png')

%%% (d) Implementacao do Overlap Save %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tamanho do comprimento da subsequencia
L = 4;
% Matriz com os valores das sequencias x_i[n]
x_m = [];
% Matriz com os valores das sequencias y_i[n] = x_i[n]*h[n]
y_m = [];
% Saida do sistema
y = [];
% DFT(h[n])
H = fft(h,L);

x_i = zeros(1,L);

% inicializando indices do comeco e fim
i_i = -(P-2);
i_f = i_i + L -1;

while (i_i <= M)    
    if (i_i < 1)
        x_i = [zeros(1,abs(i_i)+1) x(1:i_f)];
    elseif (i_f > M)
        x_i = [x(i_i:end) zeros(1,i_f-M)];
    else
        x_i = x(i_i:i_f);
    end

    i_i = (i_f - P + 2);
    i_f = i_i + L -1;
    
    x_m = [x_m; x_i];
    
    %y_i = cconv(x_i,h,L);
    y_i = abs(ifft(fft(x_i).*H));
    y_m = [y_m;y_i];
    
    y = [y y_i(P:end)];
end
% Saida atraves do overlap save
y_save = y
figure(8); stem(y_save); title('(d) Convolucao atraves de Overlap Save - y[n]');
print('imagens/d.png')

% Apresentados os dados das operacoes de Overlap Save
y_mm = zeros(rows(y_m),length(y)+P);
a = size(y_mm)(2);
for i = 1:rows(y_m)
    y_mm(i,((L-P+1)*i):((L-P+1)*i+L-1)) = y_m(i,:);
    
    figure(9);
    subplot(rows(y_mm),1,i);
    stem(x_m(i,:));
    title (sprintf ("x_{%d}[n]", i));
    
    figure(10);
    subplot(rows(y_mm),1,i);
    stem(y_mm(i,:))
    axis([1 a])
    title (sprintf ("y_{%d}[n]", i));
endfor
figure(9);print('imagens/dx.png')
figure(10);print('imagens/dy.png')

