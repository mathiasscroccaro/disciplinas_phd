pkg load signal

close all;
clear all;

x = 2*ones(1,30);

h = [1 zeros(1,11)];

x_alias = cconv(x,h,12);

X_alias = fft(x_alias,4098);
X = fft(x,4098);

w_x = linspace(0,pi,4098);
w_x_alias = linspace(0,pi,4098);

figure;
plot(w_x,abs(X));
hold on;
plot(w_x_alias,abs(X_alias));