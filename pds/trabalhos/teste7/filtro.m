clear all;
close all;

pkg load control
pkg load signal

[x, fval, info] = fsolve (@f, [1])

omega_c = x;
%omega_c = 0.5832;
N = 3;
%N = 5;

sk = [omega_c*exp(j*2*pi/3) -omega_c omega_c*exp(j*4*pi/3)];
%sk = -[omega_c*exp(j*3*pi/5) omega_c*exp(j*4*pi/5) -omega_c omega_c*exp(j*6*pi/5) omega_c*exp(j*7*pi/5)];

den = conv([1 -sk(1)],[1 -sk(2)]);
den = conv(den,[1 -sk(3)]);
%den = conv(den,[1 sk(4)]);
%den = conv(den,[1 sk(5)]);

H = tf([omega_c^N],real(den))

[num,den] = tfdata(H);
num = cell2mat(num);
den = cell2mat(den);
figure; zplane(num,den)

G = c2d(H,2,'tustin')

[num,den] = tfdata(G);
num = cell2mat(num);
den = cell2mat(den);

[h,w] = freqz(num,den);

[diff,index1] = min(abs(w-0.3*pi));
[diff,index2] = min(abs(w-0.7*pi));

hc = abs(h(index1))
hr = abs(h(index2))

figure; plot(w,abs(h)) 
annotation('textarrow',[0.4+0.1 0.35],[0.9/1.4+0.1 0.93/1.4],'string','|H(e^{j0,3\pi})| = 0,9542','fontsize',14)
annotation('textarrow',[0.6+0.05 0.6],[0.25/1.4+0.05 0.23/1.4],'string','|H(e^{j0,7\pi})| = 0,0567','fontsize',14)
title("|H(e^{ j w })|")
ylabel("Magnitude")
xlabel("Frequencia")
print("h.pdf")