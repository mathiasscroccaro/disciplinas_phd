function y = f (x)
  y = zeros (1, 1);
  %y(1) = 2*x(1)*(log10(tan(0.3*pi/2)) - log10(x(2))) - log10((1/0.89125)^2 - 1);
  %y(2) = 2*x(1)*(log10(tan(0.7*pi/2)) - log10(x(2))) - log10((1/0.056234)^2 - 1);
  y(1) = 2*3*(log10(tan(0.7*pi/2)) - log10(x(1))) - log10((1/0.056234)^2 - 1);
endfunction