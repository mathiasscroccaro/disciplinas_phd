clear all;
close all;

M = 1;
L = 2;

[y,Fs]=audioread('train.wav');

espectro(y);
soundsc(y);

y_e = zeros(1,L*length(y));

% Expansor
y_e(1:L:length(y_e)) = y(1:length(y));

% Filtro passa baixas
w_c = min([pi/M pi/L]);
h = filtro(w_c,w_c+0.1);
y_i = filter(h,1,y_e);

% Compressor
y_d = y_i(1:M:length(y_i));

espectro(y_d);
soundsc(y_d);