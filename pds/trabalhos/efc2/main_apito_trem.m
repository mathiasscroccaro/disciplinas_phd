clear all;
close all;

M = 1;
L = 3;

[y,Fs]=audioread('train.wav');

% Espectro e som do audio original
espectro(y);
title("Espectro do audio original")
soundsc(y);

y_e = zeros(1,L*length(y));

% Expansor
y_e(1:L:length(y_e)) = y(1:length(y));

% Filtro passa baixas
w_c = min([pi/M pi/L]);
h = filtro(w_c-0.1,w_c);
y_i = conv(h,y_e);

% Compressor
y_d = y_i(1:M:length(y_i));

% Espectro e som do audio subamostrado ou interpolado
espectro(y_d);
title("Espectro do audio - interpolado L=3")
soundsc(y_d);