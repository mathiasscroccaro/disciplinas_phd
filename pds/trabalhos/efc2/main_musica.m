clear all;
close all;

[y,Fs]=audioread('creed_my_sacrifice.wav');
y=y(:,1)+y(:,2);

% (d) Mostrando o espectro do sinal de audio
espectro(y);
title("Audio original")

% (e) Reduzindo a taxa de amostragem (M=6) e mostrando espectro
M=6;
y_d_no_filter = y(1:M:length(y));
espectro(y_d_no_filter);
title("Subamostrado (M=6) - sem filtro");

% (f) Reproducao do sinal subamostrado e original
soundsc(y,Fs);
soundsc(y_d_no_filter,Fs/M);

% (g) Filtragem do sinal original
w_c = pi/M;
h = filtro(w_c-0.1,w_c);
y_d = conv(h,y);

espectro(h);
title("Espectro do filtro");
espectro(y_d);
title("Audio filtrado");

soundsc(y,Fs);
soundsc(y_d,Fs);

% (h) Reducao da taxa de amostragem (M=6) 
y_d = y_d(1:M:length(y_d));

espectro(y_d);
title("Subamostrado (M=6) - com filtro");
soundsc(y_d,Fs/M);