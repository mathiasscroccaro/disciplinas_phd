\documentclass{article}[12pt]
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage[portuguese]{babel}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{amsmath}
\usepackage{steinmetz}

%%% --- HIPERLINKS --- %%%

\usepackage[colorlinks=true,plainpages=true,citecolor=black,linkcolor=black, urlcolor=blue]{hyperref}

\usepackage{indentfirst}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage[a4paper,
 top=3cm, 
 bottom=2cm, 
 left=3cm, 
 right=2cm]{geometry} 
 
\usepackage{titlesec}
\usepackage{helvet}

\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}


\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

%%% --- CABEÇALHO E RODAPÉ --- %%%

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}

\fancyhead[L]{\footnotesize Exercício de fixação 1}
\fancyhead[R]{\footnotesize IE550}   
\fancyfoot[R]{\footnotesize Unicamp} 
\fancyfoot[C]{\thepage} 
\fancyfoot[L]{\footnotesize Mathias Scroccaro Costa} 
\renewcommand{\footrulewidth}{0.7pt}
\renewcommand{\headrulewidth}{0.7pt}
 

%\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\baselinestretch}{1.5} 
\titlespacing{\section}{0pt}{36pt}{24pt}
\setlength{\parindent}{4em}


\title{IE550 - Exercício de fixação 1}

\def \proporcaoFigura {0.8}

\begin{document}

\begin{titlepage}
\begin{center}

\Large

\vspace{3cm}
IE550 - Exercício de fixação 1
\Large

\vspace{5cm}

\textbf{MATHIAS SCROCCARO COSTA}

\vspace{7cm}

\today

\vspace{4cm}

\large
Faculdade de Engenharia Elétrica e Computação – FEEC\\
Departamento de Semicondutores, Instrumentos e Fotônica – DSIF\\
UNICAMP, Campinas
\vspace*{\fill}
\end{center}
\end{titlepage}

\newpage

\normalsize

\section*{Convolução}

\subsection*{Parte Teórica}

\subsubsection*{(a)}

Segundo \cite{10.5555/1795494}, página 661, dado um sinal de entrada $x[n]$, com comprimento $K$, e a resposta ao impulso de um sistema $h[n]$, com comprimento $D$, o tamanho máximo $P$ de uma sequência resultante $y[n] = x[n]*h[n]$ (convolução linear) é de $P=(K+D-1)$. O autor ainda mostra de maneira gráfica esta conclusão, através de um exemplo.

\subsubsection*{(b)}

O vetor \textbf{x} corresponde ao vetor transposto do sinal de entrada, de maneira que 
\begin{equation*}
x = \Big[x[0]~x[1]~x[2]~\cdots~x[K-1]\Big]^{T}.
\end{equation*}

A matriz \textbf{H} apresenta um padrão singular, cujas diagonais são preenchidas com os valores da resposta ao impulso do sistema, de maneira que

\begin{equation*}
H_{P,K} = 
\begin{pmatrix}
h[0] & 0 & \cdots & 0 &  \\
h[1] & h[0] & \cdots & 0 &  \\
\vdots  &  h[1] & \ddots & \vdots  &  \\
h[D-1] & \vdots & \ddots & h[0] &  \\
0 & h[D-1] &  & h[1] &  \\
\vdots & 0 & \ddots & \vdots &  \\
0 & \cdots & 0 & h[D-1] &  \\  
\end{pmatrix}.
\end{equation*}

\

Assim, quando realizamos a operação \textbf{y} = \textbf{Hx}, temos uma convolução:
\begin{align*}
y[0] ~&=~ h[0] \times x[0] ~+~ 0 \times x[1] ~+~ \cdots ~+~ 0 \times x[K-1]\\
y[1] ~&=~ h[1] \times x[0] ~+~ h[0] \times x[1] ~+~ 0 \times x[2] ~+~ \cdots ~+~ 0 \times x[K-1]\\
y[2] ~&=~ h[2] \times x[0] ~+~ h[1] \times x[1] ~+~ h[0] \times x[2] ~+~ 0 \times x[3] ~+~ \cdots ~+~ 0 \times x[K-1]\\
\vdots ~&=~ \vdots\\
y[n] ~&=~ \sum_{m = 0}^{K-1} h[n-m]x[m]
\end{align*}

\

\subsection*{Resposta em frequência}

\subsubsection*{(c)}

Segundo \cite{10.5555/1795494}, exemplo 2.15, quando $h[n]$ é real, $H(e^{-j\omega_0})=H^*(e^{j\omega_0})$, consequentemente, é possível utilizar uma função cossenoide para estimar a resposta em frequência de um sistema LIT, varrendo os valores de $\omega$. Desta maneira, para uma entrada $x[n] = A \cos(\omega n + \phi)$, temos a saída
\begin{equation}
y[n] = A|H(e^{j \omega_0})| \cos \Big(\omega_0 n + \phi + \phase{H(e^{j \omega_0})} \Big).
\end{equation}


A estratégia adotada para determinação da resposta em frequência foi a inserção de uma entrada cossenoidal de amplitude unitária no sistema e o monitoramento da magnitude em sua saída. A entrada cossenoidal possui um $\omega$ que é varrido de 0 a $\pi$ rad. A ideia é utilizar esta função de entrada como autofunção para o sistema. O algoritmo do procedimento segue a lógica sequencial:
\begin{enumerate}
\item Inicializa as variáveis e vetores, com $\omega = 0$
\item Realiza a convolução $y[n] ~=~ cos(w*n)*h[n]$;
\item Calcula a magnitude observada $y[n]/x[n]$ e salva o valor.
\item Aumenta o valor de $w$, de maneira que $\omega_{atual} = \omega_{passado} + \pi/50$;
\item Pula ao passo (2), até $\omega == \pi$.
\end{enumerate}

\subsubsection*{(d)}

O cuidado a ser tomado com o procedimento foi de utilizar um passo entre o intervalo de varredura de $\omega$ suficientemente pequeno para visualizar detalhes da curva de resposta em frequência. Para encontrar a resposta de duas frequências específicas, $\omega_1 = \pi/5$ e $\omega_2 = \pi/10$, foi realizado o procedimento descrito no item anterior. As formas de onda da entrada e saída podem ser observadas na Figura \ref{fig:ondas}. Os valores encontrados foram de $|H(e^{j\frac{\pi}{5}})| = 0,973$ e $|H(e^{j\frac{\pi}{10}})| = 2,001$.

 \begin{figure}[!h]
 \centering
   \includegraphics[width=\textwidth]{ondas.png}
   \caption{Formas de onda da entrada e saída do sistema, para as frequências $\omega_1 = \pi/5$ e $\omega_2 = \pi/10$.}
   \label{fig:ondas}
 \end{figure}

\subsubsection*{(e)}

Código fonte está anexado ao final do trabalho.

\subsubsection*{(f)}

A Figura \ref{fig:magnitude} mostra a resposta em frequência do sistema desconhecido. O comportamento visto na curva se assemelha a um filtro passa-baixas, de maneira a possuir um ganho próximo a frequência de corte.

 \begin{figure}[!h]
 \centering
   \includegraphics[width=0.7\textwidth]{magnitude.png}
   \caption{Magnitude da resposta em frequência do sistema desconhecido.}
   \label{fig:magnitude}
 \end{figure}

\newpage
\clearpage
\pagebreak

 
\section*{ANEXO - código fonte}

\lstinputlisting[language=Matlab]{../resposta.m}

\bibliography{bibli}
\bibliographystyle{ieeetr}





\end{document}
