clear all;
close all;

N = 1000;
n = 0:1:N;

%%% Duas frequencias escolhidas: pi/5 e pi/5

w1 = pi/5;
w2 = pi/10;

x1 = cos(w1*n);
x2 = cos(w2*n);

y1 = sistema_desconhecido(x1);
y2 = sistema_desconhecido(x2);

abs_w1 = abs(y1(500)/x1(500));
abs_w2 = abs(y2(500)/x2(500));

figure(1);
hold on;
subplot(2,2,1);
plot(x1);
axis([0 1000 -1.5 1.5])
title('Entrada cos(\pi/5)');
subplot(2,2,2);
plot(x2);
axis([0 1000 -1.5 1.5])
title('Entrada cos(\pi/10)');
subplot(2,2,3);
plot(y1);
axis([0 1400 -1.5 1.5])
title('Saida para entrada cos(\pi/5)');
subplot(2,2,4);
plot(y2);
axis([0 1400 -2 2])
title('Saida para entrada cos(\pi/10)');

printf("Para w = pi/5, |H(e^-jw)| = %d\n",abs_w1);
printf("Para w = pi/10, |H(e^-jw)| = %d\n\n",abs_w2);

%%% Resposta em frequencia, varrendo w [0,pi]

H = [];

for w = 0:pi/50:pi

  x = cos(w*n);
  y = sistema_desconhecido(x);
  
  % E' utilizada uma amostra atrasada para evitar transiente do janelamento
  % da funcao cossenoidal
  abs_H = abs(y(500)/x(500));
  
  H = [H abs_H];
    
endfor

figure(2);
plot(0:pi/50:pi, H);
ylabel('|Y/X| ');
xlabel('\omega [rad]');
title('Magnitude da resposta em frequencia')

% Comparacao da resposta ao impulso e FFT
figure(3);
x2 = [1 zeros(1,1000)];
y2 = sistema_desconhecido(x2);
plot(abs(fft(y2)));