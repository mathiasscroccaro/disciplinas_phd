%
%   Script teste para convolucaoo atraves de matrizes.
%
%   Autor: Mathias Scroccaro Costa
%   Data: 23/03/2020
%

clear all;
close all;

% Vetor de entrada
x = [1 2 3 4 5 6 7 8];

% Vetor da resposta ao impulso;
h = [1 2 3 4];

% Primeiro metodo de convolucao
y1 = conv(x,h)

% Segundo metodo de convolucao - atraves de matrizes
h = [h zeros(1,length(x)-1)];
H = toeplitz(h,[h(1) zeros(1,length(x)-1)])
y2 = H*x'

% Resultados
figure;
hold on;
plot(y1,'o');
plot(y2,'x');
legend('Padrao','Matricial');